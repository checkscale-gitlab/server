# Source the node
. /${PACKAGE_NAME}/install/local_setup.sh

# Launch the `server` node
ros2 run ${PACKAGE_NAME} server
