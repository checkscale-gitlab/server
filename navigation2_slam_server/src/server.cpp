// Copyright 2019 ROB768

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "navigation2_slam_server/srv/start_navigation2.hpp"
#include "navigation2_slam_server/srv/stop_navigation2.hpp"
#include "rclcpp/rclcpp.hpp"

using namespace std::placeholders;
using StartNavigation2Srv = navigation2_slam_server::srv::StartNavigation2;
using StopNavigation2Srv = navigation2_slam_server::srv::StopNavigation2;

const std::string NODE_NAME = "navigation2_slam_server";
const std::string SRV_NAME_START = NODE_NAME + "/start";
const std::string SRV_NAME_STOP = NODE_NAME + "/stop";

struct NavigationProcess {
  uint8_t ros_domain_id;
  FILE* pipeline;
  int pid;
};

class Navigation2Server : public rclcpp::Node {
  /* Properties */
 private:
  std::shared_ptr<rclcpp::Service<StartNavigation2Srv>> srv_server_start_;
  std::shared_ptr<rclcpp::Service<StopNavigation2Srv>> srv_server_stop_;
  static inline std::vector<NavigationProcess> active_pipelines_;

  /* Methods */
 public:
  Navigation2Server();

 private:
  void srv_handler_start(
      const std::shared_ptr<rmw_request_id_t> request_header,
      const std::shared_ptr<StartNavigation2Srv::Request> request,
      const std::shared_ptr<StartNavigation2Srv::Response> response);
  void srv_handler_stop(
      const std::shared_ptr<rmw_request_id_t> request_header,
      const std::shared_ptr<StopNavigation2Srv::Request> request,
      const std::shared_ptr<StopNavigation2Srv::Response> response);
  static void signal_handler(int signal);
};

Navigation2Server::Navigation2Server() : Node(NODE_NAME) {
  // Register signals for gracefull shutdown
  std::signal(SIGTERM, Navigation2Server::signal_handler);
  // Create services
  srv_server_start_ = this->create_service<StartNavigation2Srv>(
      SRV_NAME_START,
      std::bind(&Navigation2Server::srv_handler_start, this, _1, _2, _3));
  srv_server_stop_ = this->create_service<StopNavigation2Srv>(
      SRV_NAME_STOP,
      std::bind(&Navigation2Server::srv_handler_stop, this, _1, _2, _3));

  RCLCPP_INFO(this->get_logger(), "Node `%s` started successfuly",
              NODE_NAME.c_str());
}

void Navigation2Server::srv_handler_start(
    const std::shared_ptr<rmw_request_id_t> request_header,
    const std::shared_ptr<StartNavigation2Srv::Request> request,
    const std::shared_ptr<StartNavigation2Srv::Response> response) {
  (void)request_header;
  RCLCPP_INFO(this->get_logger(),
              "Received `%s` request with `ROS_DOMAIN_ID=%d",
              SRV_NAME_START.c_str(), request->ros_domain_id);

  // Make sure the `ROS_DOMAIN_ID` is valid
  if (request->ros_domain_id > 232) {
    RCLCPP_WARN(this->get_logger(),
                "Request `%s` for `ROS_DOMAIN_ID=%d` failed because "
                "`ROS_DOMAIN_ID` larger than 232 is invalid",
                SRV_NAME_START.c_str(), request->ros_domain_id);
    response->confirmation = false;
    return;
  }

  // Make sure there is no `navigation2` already running on the particuls
  // `ROS_DOMAIN_ID`
  for (NavigationProcess process : active_pipelines_) {
    if (process.ros_domain_id == request->ros_domain_id) {
      RCLCPP_WARN(
          this->get_logger(),
          "Request `%s` for `ROS_DOMAIN_ID=%d` failed because there is already "
          "a `navigation2` process running on the given `ROS_DOMAIN_ID`",
          SRV_NAME_START.c_str(), request->ros_domain_id);
      response->confirmation = false;
      return;
    }
  }

  // Create command used to create `navigation2` subprocess
  std::string command =
      // Set the correct `ROS_DOMAIN_ID`
      "ROS_DOMAIN_ID=" + std::to_string(request->ros_domain_id) +
      // Bringup `navigation2_slam`
      // " ros2 launch navigation2_slam_server navigation2_slam_launch.py"
      " /bin/bash /navigation2_slam_server/launch/navigation2_slam_launch.bash"
      // Echo PID (required for `stop` service)
      " & echo ${!}";

  // Create subprocess for `navigation2`
  NavigationProcess navigation_process;
  navigation_process.pipeline = popen(command.c_str(), "r");
  // Save PID of this process
  fscanf(navigation_process.pipeline, "%d", &navigation_process.pid);

  // Make sure the process could be started
  if (!navigation_process.pipeline) {
    RCLCPP_WARN(this->get_logger(),
                "Request `%s` for `ROS_DOMAIN_ID=%d` failed",
                SRV_NAME_START.c_str(), request->ros_domain_id);
    response->confirmation = false;
  } else {
    RCLCPP_INFO(
        this->get_logger(), "Request `%s` for `ROS_DOMAIN_ID=%d` succeded",
        SRV_NAME_START.c_str(), request->ros_domain_id, navigation_process.pid);
    navigation_process.ros_domain_id = request->ros_domain_id;
    active_pipelines_.push_back(navigation_process);
    response->confirmation = true;
  }
}

void Navigation2Server::srv_handler_stop(
    const std::shared_ptr<rmw_request_id_t> request_header,
    const std::shared_ptr<StopNavigation2Srv::Request> request,
    const std::shared_ptr<StopNavigation2Srv::Response> response) {
  (void)request_header;
  RCLCPP_INFO(this->get_logger(),
              "Received `%s` request with `ROS_DOMAIN_ID=%d`",
              SRV_NAME_STOP.c_str(), request->ros_domain_id);

  // Iterate over all `navigation2` processes
  for (uint8_t i = 0; i < active_pipelines_.size(); i++) {
    // Find the one that is requested for termination
    if (active_pipelines_[i].ros_domain_id == request->ros_domain_id) {
      // Terminate this process
      if (kill(active_pipelines_[i].pid, SIGTERM) == -1) {
        RCLCPP_WARN(this->get_logger(),
                    "Request `%s` for `ROS_DOMAIN_ID=%d` failed",
                    SRV_NAME_STOP.c_str(), request->ros_domain_id);
        response->confirmation = false;
      } else {
        RCLCPP_INFO(this->get_logger(),
                    "Request `%s` for `ROS_DOMAIN_ID=%d` exited successfuly",
                    SRV_NAME_STOP.c_str(), request->ros_domain_id);
        active_pipelines_.erase(active_pipelines_.begin() + i);
        response->confirmation = true;
      }
      return;
    }
  }

  RCLCPP_WARN(this->get_logger(),
              "Request `%s` for `ROS_DOMAIN_ID=%d` is invalid as it was not "
              "started with `%s` before",
              SRV_NAME_STOP.c_str(), request->ros_domain_id,
              SRV_NAME_START.c_str());
  response->confirmation = false;
}

void Navigation2Server::signal_handler(int signal) {
  (void)signal;
  for (auto active_pipeline : active_pipelines_) {
    kill(active_pipeline.pid, SIGTERM);
  }
  active_pipelines_.clear();
}

int main(int argc, char* argv[]) {
  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<Navigation2Server>());
  rclcpp::shutdown();
  return 0;
}
