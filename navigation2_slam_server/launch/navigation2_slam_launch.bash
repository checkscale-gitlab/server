# Copyright 2019 ROB768

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# This bash script is used instead of proper launch script because `cartographer` complains about
# receiving `--ros-args`, which is an issue that appears by launching it from different distro that
# it was built for

# Forward `SIGTERM` to all subprocesses
forward_sigterm() {
  echo "INFO: Caught \`SIGTERM\`, forwarding to all subprocesses"
  for pid in ${subprocess_pid[*]}; do
    kill -TERM "${pid}" &>/dev/null
  done
}
# Set the trap for `SIGTERM` and `SIGINT`
trap 'forward_sigterm' SIGTERM SIGINT

# Launch `navigation2` for path planning
ros2 launch nav2_bringup nav2_navigation_launch.py &
subprocess_pid[0]="${!}"

# Launch `cartographer` for SLAM
ros2 run cartographer_ros cartographer_node -configuration_directory /${PACKAGE_NAME}/config -configuration_basename turtlebot3_lds_2d.lua &
subprocess_pid[1]="${!}"

# Launch `occupancy_grid` for building the map
ros2 run cartographer_ros occupancy_grid_node -resolution 0.05 -publish_period_sec 1.0 &
subprocess_pid[2]="${!}"

# (Optional) Launch `randon_explorer`
# ros2 run ${PACKAGE_NAME} random_explorer &
# subprocess_pid[3]="${!}"

# Wait for all subprocesses
for pid in ${subprocess_pid[*]}; do
  wait "${pid}"
done
exit 0
