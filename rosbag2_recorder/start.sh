# Copyright 2019 ROB768

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# Timestamp in the default format for marking the rosbags
timestamp=$(date +'%Y_%m_%d-%H_%M_%S')

# Forward `SIGINT` to 'ros2 bag record' once `SIGTERM` is received
sigterm_to_sigint() {
  echo "INFO: Caught \`SIGTERM\`, forwarding \`SIGINT\` to all \`ros2 bag record\`"
  # Send `SIGINT` to all subprocesses
  for pid in ${subprocess_pid[*]}; do
    kill -INT "${pid}" &>/dev/null
  done

  # Remove all recordings smaller than `VALID_ROSBAG_SIZE_KB`. This removes recordings that do not contain any data
  find "${PERSISTENT_STORAGE_DIR}" -depth -mindepth 1 -maxdepth 2 -type d | grep -v ^\\.$ | xargs -n 1 du -s | while read size name; do
    if [ "$size" -lt "${VALID_ROSBAG_SIZE_KB}" ]; then
      rm -rf $name &>/dev/null
    fi
  done
}
# Set the trap for `SIGTERM`
trap 'sigterm_to_sigint' SIGTERM

# Record topics on a specific domain using `ros2 bag record`, takes `ROS_DOMAIN_ID` as an argument
record_single_ros_domain_id() {
  # Make sure the `ROS_DOMAIN_ID` is valid
  if [[ "${1}" -lt "0" || "${1}" -gt "232" ]]; then
    echo "ERROR: All \`ROS_DOMAIN_ID\` must be in interval [0, 232]"
    exit 1
  fi

  # Get the output file path for the rosbag
  output_file_path="${PERSISTENT_STORAGE_DIR}/${DOMAIN_PREFIX}${1}/${timestamp}"

  # Set the domain id and create an appropriate 'ros2 bag record' subprocess
  ROS_DOMAIN_ID="${1}" ros2 bag record "${TOPICS_TO_RECORD}" -o "${output_file_path}" >/dev/null &

  # Save PIDs for the subprocesses, used for forwarding `SIGINT`
  subprocess_pid[${1}]="${!}"
}

# Execution starts here
if [[ "${DOMAINS_TO_RECORD}" =~ "-" ]]; then
  # Iterate over range of `ROS_DOMAIN_ID`
  IFS=- read left_boundary right_boundary <<<"${DOMAINS_TO_RECORD}"
  for id in $(seq "${left_boundary}" "${right_boundary}"); do
    record_single_ros_domain_id "${id}"
  done
  echo "INFO: Recording topics \`"${TOPICS_TO_RECORD}"\` for \`ROS_DOMAIN_ID\` between "${left_boundary}" and "${right_boundary}" (inclusively)"
else
  # Iterate over list of `ROS_DOMAIN_ID`
  for id in $(echo "${DOMAINS_TO_RECORD}" | sed "s/,/ /g"); do
    record_single_ros_domain_id "${id}"
    echo "INFO: Recording topics \`"${TOPICS_TO_RECORD}"\` with \`ROS_DOMAIN_ID\` of "${id}""
  done
fi

# Wait for all subprocesses
for pid in ${subprocess_pid[*]}; do
  wait "${pid}"
done
exit 0
