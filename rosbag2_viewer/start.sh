# If desired, set `ROS_DOMAIN_ID` according to `HOSTNAME` but keep only digits
if [ "${ROS_DOMAIN_ID_BASED_ON_HOSTNAME}" = "true" ]; then
    # Export with only digits
    export ROS_DOMAIN_ID=$(echo "${HOSTNAME}" | tr -cd '[[:digit:]]')

    # Make sure the `ROS_DOMAIN_ID` is valid
    if [[ "${ROS_DOMAIN_ID}" -lt "0" || "${ROS_DOMAIN_ID}" -gt "232" ]]; then
        echo "ERROR: \`ROS_DOMAIN_ID\` must be in interval [0, 232]"
        exit 1
    fi
fi

# Start writable 'gotty' at port 80 with `ui.sh` script
/root/go/bin/gotty -p 80 -w /bin/bash /"${PACKAGE_NAME}"/ui.sh
