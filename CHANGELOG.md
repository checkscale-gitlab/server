# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.1]
### Modified
- Provide `navigation2` service with `cartographer` SLAM instead of `amcl`

## [0.1.0]
### Added
- `navigation2_server` that provides `navigation2` with amcl from the server
- `rosbag2_recorder` that records selected topic on specified `ROS_DOMAIN_ID`s
- `rosbag2_vievew` that provides forwarding of konsole applications to port 80 
